Project Setup
=========================

Tested on Ubuntu 18.04 as host, Vagrant version 2.0.2, Vistualbox version 5.2.10, ansible 2.5.1

Tested for PHP verions: 7.3, 7.2, 7.1, 7.0, 5.6

Tested for Symfony verions on Nginx and Apache: 4.2(PHP 7.3, 7.2, 7.1), 3.4(PHP 7.2, 7.0), 3.3(PHP 7.2), 3.1(PHP 7.1), 3.0(PHP 5.6), 2.8(PHP 7.2, 5.6)

Tested for webservers: apache, nginx

Worked on Vagrant Boxes: "geerlingguy/ubuntu1804", "geerlingguy/ubuntu1604"

Start Project
============================
1. Create folder and copy ansible and Vagrantfile (if you already have project then copy them to location of folder with project code)
2. Setup Vagrantfile(IP address(you can use already predefine if not exists same in /etc/hosts) and additional provision - describe in text below in section IMPORTANT)
3. Setup playbook.yml variable describe in Project configuration
4. Type command: vagrant up
5. Copy IP address or vh into browser
6. Enjoy ;)

Project configuration
=========================
To make configuration process more userfrandly all variable for ROLEs are defined in defaults folder for specific role. Only variables defined in playbook.yml should be changed. 

Varible which you need(you can use alrady predefine) to set before start project:

  - project_name:  name of the folder with project code(this folder will be in same location as Vagrantfile and ansible folder)

  - vh: virtual host which you will use etc. symfony.test

  - vm_ip: IP address must be the same as IP address from Vagrantfile (it is good pratice to check on host machine in /etc/hosts if that ip address is not in use)

  - mysql_db: database name

  - mysql_user: database user

  - mysql_pass: database password

  - webserver: must be defined as nginx or apache

  - symfony_version: choose Symfony version which need to be installed(example 2.8; 3.4; 4.2 etc.). IMPORTANT: For existing project set real Symfony version before start vagrant up

  - php_version: set PHP version(7.3, 7.2, 7.1, 7.0, 5.6)

IMPORTANT 
===========================
When you use VM with Ubuntu17 or Ubuntu18 (in our case "geerlingguy/ubuntu1804") on some OS it may happen that you get vagrant error with interface. In that case follow next steps: 
1. add code bellow in Vagrantfile, 
2. vagrant destroy 
3. vagrant up

  # Ansible provisioning

  config.vm.provision "apt-get",

    type: "shell",

    preserve_order: true,

    inline: "apt-get -y update"

  config.vm.provision "install ifupdown",

    type: "shell",

    preserve_order: true,

    inline: "apt-get install ifupdown -y"

  config.vm.provision "ansible" do |ansible|

    ansible.verbose = "v"

    ansible.playbook = "ansible/playbook.yml"

  end

NOTICE
=============================
If you want to use this playbook for no Symfony projects then follow next steps:

1. In playbook.yml file you need to comment ROLE - symfony

2. Comment symfony_version variable 

3. Define new variable document_root.  Example of document_root variable you can see  in text below

    document_root: /var/www/html/magento 

    or
    
    document_root: /var/www/html