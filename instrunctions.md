Da bi se pokrenuo projekat potrebno je ispratiti sledece korake:
1. pokrenuti komandu vagrant up u terminalu na root-u na kome se nalazi Vagrantfile
2. nakon izvrsenja komand podesit virtual hosts komandom: sudo nano /etc/hosts (ako si Ubuntu ili iMac) i dodati(bar jednu od ove dve):
	192.168.33.202  villa-saturea.local
	192.168.33.202  www.villa-saturea.local

3. potrebno je kreirati admin user u bazi sledecim komandama:
	vagrant ssh
	cd /var/www/html/villa_saturea
	bin/console doctrine:migrations:migrate
	bin/console app:user:create admin@gmail.com asdasdasd ROLE_SUPER_ADMIN(gde je admin email  admin@gmail.com i password = asdasdasd)
