<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class HomepageController
 *
 * @package    App\Controller
 * @subpackage App\Controller\HomepageController
 */
class HomepageController extends AbstractController
{
    const TEMPLATE = '/frontend/homepage.twig';

    public function homepage(): Response
    {
        return $this->render(
            self::TEMPLATE,
            [
                'testVariable' => 'test',
            ]
        );
    }
}
