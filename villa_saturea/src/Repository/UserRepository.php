<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 *
 * @package    App\Repository
 * @subpackage App\Repository\UserRepository
 */
class UserRepository extends EntityRepository
{
}
