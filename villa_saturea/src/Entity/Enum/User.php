<?php

declare(strict_types=1);

namespace App\Entity\Enum;

/**
 * Class User
 *
 * @package    App\Entity\Enum
 * @subpackage App\Entity\Enum\User
 */
abstract class User
{
    const DEFAULT_ROLE = 'ROLE_USER';
    const ADMIN_ROLE   = 'ROLE_SUPER_ADMIN';


    /**
     * @return array
     */
    public static function getUserRolesChoicesFlip(): array
    {
        return array_flip(self::getUserRolesChoices());
    }

    public static function getUserRolesChoices(): array
    {
        return [
            self::DEFAULT_ROLE => "ROLE_USER",
            self::ADMIN_ROLE   => "ROLE_SUPER_ADMIN",
        ];
    }

}
