<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\UserCommandUtil;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

abstract class AbstractUserCommand extends ContainerAwareCommand
{
    protected $userUtil;

    public function __construct(UserCommandUtil $userUtil)
    {
        $this->userUtil = $userUtil;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setHelp($this->getHelpText());
    }

    abstract protected function getHelpText(): string;
}
